// app.tsx

import React from 'react';
import './App.css';

import Book from './pages/Book/Book.component';

const App: React.FC = () => {
  return <Book/>
};

export default App;
