import React, {useState} from 'react';
import './Book.component.css'
import CoverPage from './components/bookPages/CoverPage/CoverPage';
import IngredientPage from './components/bookPages/IngredientPage/IngredientPage';
import ChouxPage from './components/bookPages/ChouxPage/ChouxPage';
import { IonIcon } from '@ionic/react';
import { checkmarkCircle, checkmarkCircleOutline } from 'ionicons/icons';


const Book: React.FC = () => {
    
  const [currentLocation, setCurrentLocation] = useState<number>(1);

  const openBook = () => {
    const book = document.querySelector("#book") as HTMLElement;
    book.style.transform = "translateX(50%)";
    const prevBtn = document.querySelector("#prev-btn") as HTMLElement;
    prevBtn.style.transform = "translateX(-180px)";
    const nextBtn = document.querySelector("#next-btn") as HTMLElement;
    nextBtn.style.transform = "translateX(180px)";
  };

  const closeBook = (isAtBeginning: boolean) => {
    const book = document.querySelector("#book") as HTMLElement;
    const prevBtn = document.querySelector("#prev-btn") as HTMLElement;
    const nextBtn = document.querySelector("#next-btn") as HTMLElement;

    if (isAtBeginning) {
      book.style.transform = "translateX(0%)";
    } else {
      book.style.transform = "translateX(100%)";
    }

    prevBtn.style.transform = "translateX(0px)";
    nextBtn.style.transform = "translateX(0px)";
  };

  const goNextPage = () => {
    const maxLocation = 4; // 3 papers + 1
    if (currentLocation < maxLocation) {
      switch (currentLocation) {
        case 1:
          openBook();
          const paper1 = document.querySelector("#p1") as HTMLElement;
          paper1.classList.add("flipped");
          paper1.style.zIndex = "1";
          break;
        case 2:
          const paper2 = document.querySelector("#p2") as HTMLElement;
          paper2.classList.add("flipped");
          paper2.style.zIndex = "2";
          break;
        case 3:
          const paper3 = document.querySelector("#p3") as HTMLElement;
          paper3.classList.add("flipped");
          paper3.style.zIndex = "3";
          closeBook(false);
          break;
        default:
          throw new Error("Unknown state");
      }
      setCurrentLocation(prevLocation => prevLocation + 1);
    }
  };

  const goPrevPage = () => {
    if (currentLocation > 1) {
      switch (currentLocation) {
        case 2:
          closeBook(true);
          const paper1 = document.querySelector("#p1") as HTMLElement;
          paper1.classList.remove("flipped");
          paper1.style.zIndex = "3";
          break;
        case 3:
          const paper2 = document.querySelector("#p2") as HTMLElement;
          paper2.classList.remove("flipped");
          paper2.style.zIndex = "2";
          break;
        case 4:
          openBook();
          const paper3 = document.querySelector("#p3") as HTMLElement;
          paper3.classList.remove("flipped");
          paper3.style.zIndex = "1";
          break;
        default:
          throw new Error("Unknown state");
      }
      setCurrentLocation(prevLocation => prevLocation - 1);
    }
  };

  const [isActive, setIsActive] = useState(false);

  const handleFormSubmit = () => {
    setIsActive(!isActive);
  };

  return (
    <div className=" container" id='the-app'>

      {/* message de fin */}
      
      {isActive && (
        <div className={`d-flex justify-content-center align-items-center flex-column submit-message ${isActive ? 'aparition' : ''}`}>
          <span style={{display: 'block'}}>Recette ajoutée avec succès</span>
          <IonIcon icon={checkmarkCircleOutline} color='success'  style={{fontSize: '50px', display: 'block'}}/>
        </div>
      )}
      <div id="book" className={`book ${isActive ? 'disparition' : ''}`}>
        {/* paper 1 */}
        
        <div id="p1" className="paper p1">
          <div className="front">
              <CoverPage goNextPage={goNextPage} currentLocation={currentLocation}/>
          </div> 
            <div className="back">
          {currentLocation > 1 && (
                <IngredientPage goPrevPage={goPrevPage} currentLocation={currentLocation} />
                )}
            </div>
        </div>
       
        

        {/* paper 2 */}
        <div id="p2" className="paper p2">
          <div className="front">
          {currentLocation > 1 && (
            <ChouxPage handleFormSubmit={handleFormSubmit}/>
            )}
          </div>         
        </div>

      

      </div>


    </div>
  );

};

export default Book
