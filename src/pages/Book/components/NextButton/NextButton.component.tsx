import { IonIcon } from '@ionic/react';
import { caretForwardOutline } from 'ionicons/icons';
import React from 'react';

// some css code for previous button compoennt
// import './NextButton.component.css';


interface NextButtonProps {
  goNextPage: () => void;
  disabled: boolean;
}

const NextButton: React.FC<NextButtonProps> = ({ goNextPage, disabled }) => {
  return <button id="next-btn" onClick={goNextPage} disabled={disabled}>
    <IonIcon icon={caretForwardOutline} className="fa-hover" color="black" size='large' style={{height: "25px", opacity: "0.5"}}/>
  </button>;
}

export default NextButton;
