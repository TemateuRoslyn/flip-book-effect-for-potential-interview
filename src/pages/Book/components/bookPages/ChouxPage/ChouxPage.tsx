import React from 'react';
import './ChouxPage.component.css';

interface ChouxPageProps {
  handleFormSubmit: () => void;
}

const ChouxPage: React.FC<ChouxPageProps> = ({handleFormSubmit}) => {

  const divs = Array(10).fill(null)

  return (
    <div className="front-content">
      {/* left side */}
       <div className="cover-leftSide  h-100"/>

      {/* right side */}
       <div className="cover-rightSide h-100 d-flex flex-column">

          <div className="d-flex pt-2 cover-rightSide-item justify-content-center align-self-center align-items-conter cover-rightSide-item-header w-100" style={{height: '10%'}}>
              <div className='d-flex flex-column align-items-conter'>
                <span className='fw-bold'>Pâte à choux</span>
                <span className='text-center' style={{color: 'gray'}}>procédé</span>
              </div>
          </div>

          <div className="cover-rightSide-item cover-rightSide-item-center w-100" style={{height: '83%'}}>
              
             <div className='container-procede d-flex flex-column justify-content-center align-self-center align-items-center'>
             {divs.map((value, index) => (
              <div key={Math.random().toString()} className='procede-line d-flex bg-transparent'>
                
                <div className="procede-line-item line-item1"><span style={{textAlign: 'center'}}>{index+1}</span></div>
                <div className="procede-line-item line-item2"></div>
              </div>
            ))}
              <div className='add-procede-btn'>
                <span>+</span>
              </div>
             </div>
              

          </div>

          <div className="cover-rightSide-item cover-rightSide-item-footer pb-3 pt-1 d-flex flex-end" style={{height: '7%'}}>
            <div className=" w-100 d-flex justify-content-center align-self-center">
                <button className="btn btn-outline-secondary rounded-pill" style={{height: "32px", fontSize: "14px", paddingLeft: "22px", paddingRight: "22px", marginRight: '4%'}}>Annuler</button>
                <button className="btn btn-outline-success rounded-pill" style={{height: "32px", fontSize: "14px", paddingLeft: "22px", paddingRight: "22px", marginLeft: '4%'}} onClick={handleFormSubmit}>Ajouter</button>
            </div>
          </div>

          
          
       </div>

    </div>
  );
}

export default ChouxPage;
