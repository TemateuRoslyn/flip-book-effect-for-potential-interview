import React, { useState } from 'react';
import './IngredientPage.component.css';
import './timeline.css';

import PreviousButton from '../../PreviousButton/PreviousButton.component';
import { IonIcon } from '@ionic/react';
import { logoEuro, searchOutline } from 'ionicons/icons';
import { Icon } from 'ionicons/dist/types/components/icon/icon';


interface IngredientPageProps {
  goPrevPage: () => void;
  currentLocation: number
}



const IngredientPage: React.FC<IngredientPageProps> = ({goPrevPage, currentLocation}) => {
 
  const [timelineItems, setTimelineItems] = useState([
    { leftText: 'Lait entier', poids: '535 g', cout: 0.25, isCout: true},
    { leftText: 'Eau', poids: '535 g', cout: 0.25, isCout: true},
    { leftText: 'Sucre semoule', poids: '45 g', cout: 0.25, isCout: true},
    { leftText: 'Sel fin', poids: '20 g', cout: 0.25, isCout: true},
    { leftText: 'Beurre doux', poids: '470 g', cout: 0.25, isCout: true},
    { leftText: 'Farine T45 Gruau', poids: '290 g', cout: 0.25, isCout: true},
    { leftText: 'Farine T55', poids: '285 g', cout: 0.25, isCout: true},
    { leftText: 'Oeuf entiers', poids: '950 g', cout: 0.25, isCout: true},
    { leftText: 'Ingrédient ajouté', poids: '', cout: 0,  isCout: false},
    
  ])
  return (
    <div className="back-content">
      

      {/* left side */}
       <div className="ingredient-leftSide h-100 d-flex flex-column">

          <div className="ingredient-leftSide-item  ingredient-leftSide-item-header">
              <div className="header-container d-flex flex-column justify-content-center align-items-center h-100 w-100">
                <span className="ingredient d-block" style={{color: 'gray'}}>Ingrédients</span>
                
                <div className="search-container mt-1">
                  <IonIcon icon={searchOutline} className='search-btn' size='medium'/>
                  <input type="text" placeholder="" />
                </div>

              </div>
          </div>

          <div className="ingredient-leftSide-item ingredient-leftSide-item-center">
           
             <div style={{marginTop: '5%'}}>
              <div className="timeline">
                    <div className="timeline-content">
                      <div className='d-flex ingrdient-title '>
                        <div><span className="d-block ingredient-title">Ingrédient</span></div>
                        <div><span className="d-block poids-title">Poids</span></div>
                        <div><span className="d-block cout-title">Coût</span></div>
                      </div>
                      {timelineItems.map((timelineItem, index) => (
                        <div className='timeline-item d-flex mt-2' key={index}>
                            <div  className='timeline-item-left w-50 d-flex justify-content-end'><span>{timelineItem.leftText}</span></div>
                            <span className='hexa'></span>
                            <div className='timeline-item-right w-50 d-flex justify-content-start'>
                              <div className='timeline-item-right d-flex justify-content-start w-100'>
                                <div className='w-100 d-flex justify-content-between'>
                                  {timelineItem.isCout ? (
                                    <>
                                      <div className='d-flex poids'><span>{timelineItem.poids}</span></div>
                                      <div className='cout'>
                                        <span>{timelineItem.cout}</span>
                                        <IonIcon icon={logoEuro} style={{opacity: '0.7', height: '50%', marginTop: '4%'}}/>

                                        </div>
                                    </>
                                  )
                                  : 
                                    <>
                                      <div className='emptyPoid'></div>
                                      <div />
                                    </>
                                  }
                                    <div className='croix'><span>x</span></div>
                                </div>
                                
                              </div>
                            </div>
                        </div>
                      ))}
                    </div>
                </div>
             </div>

          </div>

          <div className="ingredient-leftSide-item ingredient-leftSide-item-footer">
            
            <div className="d-flex justify-content-between h-100">
                <div className="d-flex pt-4">
                    <button id="next-btn" disabled={true}/>
                    <PreviousButton disabled={currentLocation<2} goPrevPage={goPrevPage} />
                </div>

                <div className='d-flex flex-column justify-content-center'>
                    <span className='d-inline-block' style={{color: 'gray'}}>Coût au Kg</span>
                    <span className='d-inline-block' style={{fontWeight: '400', textAlign: 'center', }}>
                        <span className='d-inline-block' style={{height: '60%', fontSize: '17px'}}>3,56</span>
                       <IonIcon icon={logoEuro} style={{opacity: '0.8', height: '60%'}}></IonIcon>
                    </span>
                </div>
                <div className=''/>
            </div>

          </div>

          {/*select   justify-content-center align-self-center*/}
          {/* d-grid me-md-6  mt-md-négatif-2 col-3 mx-auto */}
          
       </div>

       {/* right side */}
       <div className="ingredient-rightSide h-100"/>

    </div>
  );
}

export default IngredientPage;
