import React from 'react';

import './Paper.component.css'

interface PaperProps {
  id: string;
  frontText: string;
  backText: string;
}

const Paper: React.FC<PaperProps> = ({ id, frontText, backText }) => {
  return (
      <>
        <div className="front">
          <div className="front-content">
            <h1>{frontText}</h1>
          </div>
        </div>
        <div className="back">
          <div className="back-content">
            <h1>{backText}</h1>
          </div>
        </div>
      </>
    
  );
};

export default Paper;