import React from 'react';

// some css code for Back button compoennt
import './Back.component.css'


interface BackProps {
  children: React.ReactNode;
}

const Back: React.FC<BackProps> = ({ children }) => {
  return <div className="back">{children}</div>;
};

export default Back
