import React from 'react';

// some css code for Front button compoennt
import './Front.component.css'


interface FrontProps {
    children: React.ReactNode;
}

const Front: React.FC<FrontProps> = ({ children }) => {
    return <div className="front">{children}</div>;
};

export default Front
