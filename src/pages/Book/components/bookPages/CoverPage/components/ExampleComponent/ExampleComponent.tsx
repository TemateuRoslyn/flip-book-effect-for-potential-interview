import React, { useState } from 'react';
import './ExampleComponent.css';
import AscenseurComponent from '../Ascenseur/Ascenseur.component';
import { IonIcon } from '@ionic/react';
import { chevronDownOutline } from 'ionicons/icons';

interface ExampleComponentProps {
  // isDropDownActiveChild: (isOpen: boolean) => void;
  // closeChild: boolean;
}

const ExampleComponent: React.FC<ExampleComponentProps> = ({}) => {

  const [selectValue, setSelectValue] = useState<string>('Unité');
  const [isDropDownActive, setIsDropDownActive] = useState(false);
  const [selectBorderRadius, setSelectPadding] = useState({
    borderBottomLeftRadius: '15px',
    borderBottomRightRadius: '15px',
  });

  const handleDropDownClick = () => {
    if(!isDropDownActive === true){
      // isDropDownActiveChild(true) // on avertis le parent que c'est ferme
      setSelectPadding((prevPadding) => ({
        ...prevPadding,
        borderBottomLeftRadius: '0px', // Valeur de padding en pixels
        borderBottomRightRadius: '0px', // Valeur de padding en pixels
      }));
    } else if(!isDropDownActive === false){
      // isDropDownActiveChild(false) // on avertis le parent que c'est ouver
      setSelectPadding((prevPadding) => ({
        ...prevPadding,
        borderBottomLeftRadius: '15px', // Valeur de padding en pixels
        borderBottomRightRadius: '15px', // Valeur de padding en pixels
      }));
    }

    setIsDropDownActive(!isDropDownActive);
    
  };
  const handleLiClick = (event: React.MouseEvent<HTMLLIElement>) => {
    // Récupérer l'élément `span` imbriqué
    const spanElement = event.currentTarget.querySelector('span.option-text');
  
    // Accéder au texte de l'élément `span`
    const optionText = spanElement?.textContent;
  
    // Traiter le texte récupéré (ex: l'afficher, le stocker, etc.)
    setSelectValue(''+optionText)
    handleDropDownClick();
  };

  return (
    <div className='d-flex flex-column justify-content-center align-self-center algn-items-center'>
      <div className='text-center'>
        
        <span className='low-opacity' style={{fontSize: '14px'}}>Conservation</span>
      </div>

      <div className="d-flex justify-content-center mt-1">
        <AscenseurComponent />
        <div className="input-group" style={{width: "22%"}}>
            
          <div className="select-menu" style={{width: '83%'}}>
            <div 
                className={isDropDownActive ? 'select-btn active' : 'select-btn'} 
                id="select-btn" 
                onClick={handleDropDownClick}
                style={{
                  borderBottomLeftRadius: selectBorderRadius.borderBottomLeftRadius,
                  borderBottomRightRadius: selectBorderRadius.borderBottomRightRadius,
                }}>
              <span id="text">{selectValue}</span>
              <IonIcon icon={chevronDownOutline} className='icon-chevron'/>
            </div>
            <div className="list" style={{
                  borderBottomLeftRadius: '15px',
                  borderBottomRightRadius: '15px',
                }}>
              <li className="option" onClick={handleLiClick}>
                <span className="option-text">kg</span>
              </li>
              <li className="option" onClick={handleLiClick} style={{
                  borderBottomLeftRadius: '15px',
                  borderBottomRightRadius: '15px',
                }}>
                <span className="option-text">g</span>
              </li>
            
            </div>
          </div>
                  
          {/* <select className="form-select form-select-sm rounded-pill shadow-none"  id="selectBackground">
            <option selected style={{fontSize: '14px'}}>Unité</option>
            <option value="kg" style={{fontSize: '14px'}}>kg</option>
            <option value="g" style={{fontSize: '14px'}}>g</option>
          </select> */}
        </div>
      </div>

      <div className="form-group mt-1 justify-content-center align-self-center algn-items-center" style={{width: '90%'}}>
        <label htmlFor="texte" className='low-opacity' style={{fontSize: '14px'}}>Notes</label>
        <textarea 
          className="form-control shadow-none" 
          style={{backgroundColor: "#F3F3F3",border: 'none', borderRadius: '20px', fontSize: '14px'}}
          id="texte" 
          name="texte" 
          rows={12}
        />
      </div>
    </div>
  );
}

export default ExampleComponent;
