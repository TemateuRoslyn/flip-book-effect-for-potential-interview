import { IonIcon } from '@ionic/react';
import { caretDownOutline, caretUpOutline, playOutline } from 'ionicons/icons';
import './Ascenseur.component.css';
import React, { useEffect, useState } from 'react';



const AscenseurComponent = () => {
  const [value, setValue] = useState(2);
  
  const [isIncrement, setisIncrement] = useState(true);
  const [displayBlackIncrement, setDisplayBlackIncrement] = useState(false);

  const [isDecrement, setisDecrement] = useState(true);
  const [displayBlackDecrement, setDisplayBlackDecrement] = useState(false);

  useEffect(() => {
    if (isIncrement === false) { // Trigger on playback stop
      setDisplayBlackIncrement(true); // Show pause icon
      const timeoutId = setTimeout(() => {
        setDisplayBlackIncrement(false)
        setisIncrement(!isIncrement)
      }, 100); // Hide pause icon after 100ms
      return () => clearTimeout(timeoutId); // Cleanup function for timeout
    } else if(isDecrement === false ){
      setDisplayBlackDecrement(true)
      const timeoutId = setTimeout(() => {
        setDisplayBlackDecrement(false)
        setisDecrement(!isDecrement)
      }, 100); // Hide pause icon after 100ms
      return () => clearTimeout(timeoutId); // Cleanup function for timeout
    }
  }, [isIncrement, isDecrement]); // Update effect when isIncrement changes

  const handleIncrement = () => {
    setValue(value + 1);
  };

  const handleDecrement = () => {
    setValue(value - 1);
  };

  return (
    <div className="d-flex align-items-center">
      <div className="d-flex flex-column">

          {isIncrement ? (
            <IonIcon
              icon={playOutline}
              className="fa-hover icon-size"
              style={{ rotate: "-90deg" }}
              onClick={() => {
                setisIncrement(!isIncrement)
                handleIncrement()
              }}
            />
          ) : displayBlackIncrement ? (
            <IonIcon icon={caretUpOutline} className="fa-hover" color="black" />
          ) : null}

          {isDecrement ? (
            <IonIcon 
              icon={playOutline} 
              className="fa-hover  icon-size" 
              style={{ rotate: "90deg" }}
              onClick={() => {
                setisDecrement(!isDecrement)
                handleDecrement()
              }}
            />
          ) : displayBlackDecrement ? (
            <IonIcon icon={caretDownOutline} className="fa-hover" color="black" />
          ) : null}

      </div>
      <input
        type="text"
        id='champTexte'
        className="form-control form-control-sm text-right shadow-none"
        value={value}
        readOnly={true}
      />
    </div>
  );
}

export default AscenseurComponent;


