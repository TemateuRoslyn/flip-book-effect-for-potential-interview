import React, { useState } from 'react';
import './toggle.component.css';

const ToggleButton: React.FC = () => {
  const [isActive, setIsActive] = useState(false);

  const toggleButton = () => {
    setIsActive(!isActive);
  };

  return (
    <div className="d-flex justify-content-center align-self-center">
      <button className={`mt-4 toggle-button ${isActive ? 'active' : ''}`} onClick={toggleButton}>
        <div className={`toggle-indicator ${isActive ? 'active' : ''}`}></div>
      </button>
      <p className="toggle-text mb-4 " style={{fontSize: '14px', color: 'gray'}}>Recette utilisée dans d'autres recettes</p>
    </div>
  );
}

export default ToggleButton;
