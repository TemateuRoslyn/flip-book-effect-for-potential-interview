import React, { useState } from 'react';
import './CoverPage.component.css';
import ToggleButton from './components/Toggle/toggle.component';
import ExampleComponent from './components/ExampleComponent/ExampleComponent';
import NextButton from '../../NextButton/NextButton.component';
import { IonIcon } from '@ionic/react';
import { chevronDownOutline } from 'ionicons/icons';



interface CoverPageProps {
  goNextPage: () => void;
  currentLocation: number
}


const CoverPage: React.FC<CoverPageProps> = ({goNextPage, currentLocation}) => {
 
  const [selectValue, setSelectValue] = useState<string>('Catégorie');
  const [isDropDownActive, setIsDropDownActive] = useState(false);
  const [isChildDropDownActive, setIsChildDropDownActive] = useState(false);
  const [selectBorderRadius, setSelectPadding] = useState({
    borderBottomLeftRadius: '15px',
    borderBottomRightRadius: '15px',
  });

  const isDropDownActiveChild = (isopen: boolean) => {
    setIsChildDropDownActive(isopen)
  }


  const closeSelect = () => {
    // if(isChildDropDownActive){
    //   //if child is open
    //   alert('OPEN')
    // }
    if(isDropDownActive === true){
      handleDropDownClick();
      
      // setSelectPadding((prevPadding) => ({
      //   ...prevPadding,
      //   borderBottomLeftRadius: '15px', // Valeur de padding en pixels
      //   borderBottomRightRadius: '15px', // Valeur de padding en pixels
      // }));
    }
  }
  
  const handleDropDownClick = () => {
    if(!isDropDownActive === true){
      setSelectPadding((prevPadding) => ({
        ...prevPadding,
        borderBottomLeftRadius: '0px', // Valeur de padding en pixels
        borderBottomRightRadius: '0px', // Valeur de padding en pixels
      }));
    } else if(!isDropDownActive === false){
      setSelectPadding((prevPadding) => ({
        ...prevPadding,
        borderBottomLeftRadius: '15px', // Valeur de padding en pixels
        borderBottomRightRadius: '15px', // Valeur de padding en pixels
      }));
    }

    setIsDropDownActive(!isDropDownActive);
    
  };
  const handleLiClick = (event: React.MouseEvent<HTMLLIElement>) => {
    // Récupérer l'élément `span` imbriqué
    const spanElement = event.currentTarget.querySelector('span.option-text');
  
    // Accéder au texte de l'élément `span`
    const optionText = spanElement?.textContent;
  
    // Traiter le texte récupéré (ex: l'afficher, le stocker, etc.)
    setSelectValue(''+optionText)
    handleDropDownClick();
  };

  return (
    <div className="front-content">
      {/* left side */}
       <div className="cover-leftSide  h-100"/>

      {/* right side */}
       <div className="cover-rightSide h-100 d-flex flex-column" onClick={closeSelect}>

          <div className="cover-rightSide-item justify-content-center align-self-center cover-rightSide-item-header">
              <div className="select-menu" style={{width: '80%', marginTop: '10%', marginLeft: '18%'}}>
                <div 
                    className={isDropDownActive ? 'select-btn active' : 'select-btn'} 
                    id="select-btn" 
                    onClick={handleDropDownClick}
                    style={{
                      borderBottomLeftRadius: selectBorderRadius.borderBottomLeftRadius,
                      borderBottomRightRadius: selectBorderRadius.borderBottomRightRadius,
                    }}>
                  <span id="text">{selectValue}</span>
                  <IonIcon icon={chevronDownOutline} className='icon-chevron'/>
                </div>
                <div className="list" style={{
                      borderBottomLeftRadius: '15px',
                      borderBottomRightRadius: '15px',
                    }}>
                  <li className="option" onClick={handleLiClick}>
                    <span className="option-text">Pâte et Biscuit</span>
                  </li>
                  <li className="option" onClick={handleLiClick}>
                    <span className="option-text">Garnitures chaudes</span>
                  </li>
                  <li className="option" onClick={handleLiClick}>
                    <span className="option-text">Garnitures froide</span>
                  </li>
                  <li className="option" onClick={handleLiClick} style={{
                      borderBottomLeftRadius: '15px',
                      borderBottomRightRadius: '15px',
                    }}>
                    <span className="option-text">Décore et autres</span>
                  </li>
                </div>
              </div>
          </div>

          <div className="cover-rightSide-item cover-rightSide-item-center">
              <div className="form-group d-flex justify-content-center align-items-center" >
                  <input type="text" className="input-border-bottom" style={{ outline: 'none', fontSize: '14px', color: 'gray'}} placeholder='Nom'/>
              </div>
              <ToggleButton/>
              <ExampleComponent/>

          </div>

          <div className="cover-rightSide-item cover-rightSide-item-footer pb-4 d-flex flex-end">
            <div className=" w-100 d-flex justify-content-center align-self-center">
              <button className="btn btn-outline-secondary rounded-pill" style={{height: "32px", fontSize: "14px", paddingLeft: "22px", paddingRight: "22px"}}>Annuler</button>
            </div>
            <div className="d-flex">
                <NextButton disabled={currentLocation>1} goNextPage={goNextPage} />
                <button id="prev-btn" disabled={true}/>
            </div>  
          </div>

          
          
       </div>

    </div>
  );
}

export default CoverPage;
