import { IonIcon } from '@ionic/react';
import { caretBackOutline } from 'ionicons/icons';
import React from 'react';

// some css code for previous button compoennt
// import './PreviousButton.component.css'


interface PreviousButtonProps {
  goPrevPage: () => void;
  disabled: boolean;
}

const PreviousButton: React.FC<PreviousButtonProps> = ({ goPrevPage, disabled }) => {
  return <button id="prev-btn" onClick={goPrevPage} disabled={disabled}>
            <IonIcon icon={caretBackOutline} className="fa-hover" color="black" size='large' style={{height: "25px", opacity: "0.5"}}/>

          </button>;
}

export default PreviousButton
